
Infinity -

Automatic - Your champion will automatically use this ability until death regardless of whether you specify
the ability to be used or not.  Affected by cooldown reduction.